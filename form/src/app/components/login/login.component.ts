import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    login: new FormControl(''),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    profile: new FormGroup({
      name: new FormControl(''),
      age: new FormControl('')
    })
  })

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.form)
  }

  submit(): void {
    if (this.form.invalid) {
      return
    }
    const {login, password} = this.form.value;
    console.log('Отправляю', login, password)
  }

}
