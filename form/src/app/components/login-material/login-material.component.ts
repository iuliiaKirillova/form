import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {passwordValidator} from "../../validators/val";

@Component({
  selector: 'app-login-material',
  templateUrl: './login-material.component.html',
  styleUrls: ['./login-material.component.scss']
})
export class LoginMaterialComponent implements OnInit {


  form: FormGroup = new FormGroup({
    account: new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
        repeatPassword: new FormControl(null, [Validators.required]),
      },
      [passwordValidator]
    ),
    profile: new FormGroup({
      name: new FormControl(null, [Validators.required]),
      phone: new FormControl(null, [Validators.required]),
      city: new FormControl()
    }),
    company: new FormGroup({
      companyName: new FormControl(null, [Validators.required]),
      formOfProperty: new FormControl('LE', [Validators.required]),
      inn: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
      kpp: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
      okpo: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      creationDate: new FormControl()
    }),
    contacts: new FormArray([])
  });

  constructor() {
  }

  ngOnInit(): void {
    this.toggleKPPValiator()
  };

  addContact(): void {
    (this.form.get('contacts') as FormArray).push(
      new FormGroup(({
          name: new FormControl(null, [Validators.required]),
          position: new FormControl(null, [Validators.required]),
          phone: new FormControl(null, [Validators.required])
        }),
      )
    )
  }

  getContactsControl(): AbstractControl[] {
    return (this.form.get('contacts') as FormArray).controls
  }

  toggleKPPValiator(): void {
    this.form.get('company.formOfProperty')?.valueChanges.subscribe(
      (form: string) => {
        if (form === 'SP') {
          this.form.get(`company.kpp`)?.clearValidators();
          this.form.get(`company.kpp`)?.reset();
          return;
        }
        this.form.get('company.kpp')?.setValidators(
          [Validators.required,
            Validators.minLength(9),
            Validators.maxLength(9)
          ]
        )
      }
    )
  }

}

