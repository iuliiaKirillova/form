import {AbstractControl, ValidationErrors} from "@angular/forms";

export const passwordValidator = (control: AbstractControl): ValidationErrors | null => {
  const p1: string = control.get('password')?.value;
  const p2: string = control.get('repeatPassword')?.value;
  const error: ValidationErrors | null = p1 === p2
    ? null
    : {passwordNotEqual: true};

  control.get('repeatPassword')?.setErrors(error)

  return error;
}
